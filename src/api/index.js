import axios from 'axios'
import configs from '../local_config'
import {setHeaderAuth} from '@/utils'

const API = configs.API
const HTTP = configs.HTTP_API
// axios.defaults.baseURL = location.protocol + '//' + API + '/api/'
axios.defaults.baseURL = HTTP + '//' + API + '/api/'
axios.defaults.timeout = 900000
setHeaderAuth()


export function authenticateEstudiante(data) {
    return axios.post('loginEstudiante', data)
}

export function authenticateAsesor(data) {
    return axios.post('loginAsesor', data)
}

export function authenticateColegio(data) {
    return axios.post('loginColegio', data)
}
export function loginAdmin(data) {
    return axios.post('login', data)
}
export function getUsers() {
    return axios.get('buscarUsuarios')
}

export function registerStudent(data) {
    return axios.post('RegistrarEstudiante', data)
}
export function postRegisterKeys(data) {
    return axios.post('postRegisterKey', data)
}
export function postRegisterAccount(data) {
    return axios.post('postRegisterAccount', data)
}

export function postUpdateKeys(data) {
    return axios.post('postUpdateKey', data)
}
export function deleteKeys(data) {
    return axios.post('deleteKey', data)
}
export function updateKeys(data) {
    return axios.post('updateKey', data)
}
export function updateAccount(data) {
    return axios.post('updateAccount', data)
}
export function updateAPassword(data) {
    return axios.post('updateAPassword', data)
}
export function deleteAccount(data) {
    return axios.post('deleteAccount', data)
}
export function sendCredentials(data) {
    return axios.post('sendCredentials', data)
}

export function actualizarEstudiante(data) {
    return axios.post('actualizarEstudiante', data)
}

export function VerifyKeys(id) {
    return axios.get('VerificarKey/' + id)
}

export function postRespuesta(data) {
    return axios.post('postRespuestas', data)
}
export function postOption(data) {
    return axios.post('postOption', data)
}
export function PostCalificarSimulcro(data) {
    return axios.post('PostCalificar', data)
}

export function getResultados(id) {
    return axios.get('getResultados/' + id)
}
export function GetStudentsKey(id) {
    return axios.get('GetStudentKey/' + id)
}

export function getPromedio(data) {
    return axios.get('getPromedio/' + data.key + '/' + data.id)
}

export function getPromedioSimulacroGraf(data) {
    console.log('aqui getPromedioSimulacroGraf->', data)
    return axios.get('getPromedioSimulacroGraf/' + data.key + '/' + data.id)
}
export function getPreguntas(data) {
    return axios.get('getPreguntas/' + data.id + "/" + data.id_carrera)
}
export function getStudiantesPorCarrera(id) {
    return axios.get('getStudiantePorCarrera/' + id)
}

export function getPuntajesMaximo(data) {
    return axios.get('getPuntajeMaximo', {
        params: data
    })
}

export function searchCollege() {
    return axios.get('getColegio')
}

export function getSimulacro() {
    return axios.get('getSimulacros')
}
export function getSimulacrosAdmin() {
    return axios.get('getSimulacrosAdmin')
}

export function GetAccounts() {
    return axios.get('GetAccounts')
}

export function searchCareer() {
    return axios.get('getCarrera')
}
export function getSecciones() {
    return axios.get('getSeccion')
}
export function getGrados() {
    return axios.get('getGrado')
}

export function getInstruccionesGenereales(id) {
    return axios.get('getInstruccionesGenereales/' + id)
}

export function getModulosxSimulacros(data) {
    return axios.get('getModulosxSimulacro/' + data.id_simulacro + '/' + data.id_carrera)
}

export function getInstruccionesModulo(id) {
    return axios.get('getInstruccionesModulo/' + id)
}
export function getInstruccionesModuloExamen(data) {
    return axios.post('getInstruccionesModuloExamen', data)
}
export function getSimulacrosXcarrera(id) {
    return axios.get('getSimulacrosXcarrera/' + id)
}
export function getAnimacionesSimulacro(id) {
    return axios.get('getAnimacionesSimulacro/' + id)
}

export function getAnimacionesResultado(id) {
    return axios.get('getAnimacionesResultado/' + id)
}
export function getOption(id) {
    return axios.get('getOption/' + id)
}

export function getDashboard (id) {
    return axios.get('getDashboarddata/' + id)
}
export function getDashboarddatas () {
    return axios.get('getDashboarddatas')
}
export function GetKeyAsesor (id) {
    return axios.get('GetKeysAsesor/' + id)
}
export function GetKeysAsesors () {
    return axios.get('GetKeysAsesores')
}
export function GetKeysAsesorAll () {
    return axios.get('GetKeysAsesorAll')
}
export function GetColegioKey (id) {
    return axios.get('GetColegiosKey/' + id)
}
export function GetColegioKeys () {
    return axios.get('GetColegiosKeys')
}
export function getPromedioSimulacros () {
    return axios.get('getPromedioSimulacro')
}

export function GetEstudiante (id) {
    return axios.get('getEstudiante/' + id)
}

export function GetKeysColegio (id) {
    return axios.get('GetKeysColegio/' + id)
}

export function GetEstudiantesStatus (id) {
    return axios.get('getEstudiantesStatus/' + id)
}

export function GetMaxMinSimulacro(data) {
    return axios.get('getMaxMinSimulacro/' + data.key + '/' + data.id)
}

export function GetNumeroEstudiante(data) {
    return axios.get('getNumeroEstudiante/' + data.key + '/' + data.id)
}

export function GetAvgCarrera(data) {
    return axios.get('getAvgCarrera/' + data.key + '/' + data.id)
}
export function getDificultModule(data) {
    return axios.get('getDificultModule/' + data.key + '/' + data.id)
}
export function getSatifaccion(data) {
    return axios.get('getSatifaccion/' + data.key + '/' + data.id)
}


export function GetBuenMalPuntaje(data) {
    return axios.get('getBuenMalPuntaje/' + data.key + '/' + data.id)
}
export function getSimulacroTipo(data) {
    return axios.get('getSimulacroTipo/' + data.id_simulacro + '/' + data.tipo)
}
export function eliminarSimulacroTipo(data) {
    return axios.get('eliminarSimulacroTipo/' + data.id_simulacro_hijo + '/' + data.tipo)
}
export function eliminarSimulacro(data) {
    return axios.get('eliminarSimulacro/' + data.id_simulacro + '/' + data.tipo)
}

export function PostSupport (data) {
    return axios.post('registrarSoporte', data)
}
export function postInfo (data) {
    return axios.post('postInfo', data)
}
export function uploadImage (data) {
    return axios.post('uploadImage', data)
}

export function updateInstruccion (data) {
    return axios.post('updateInstruccion', data)
}
export function updateInstruccionModulo (data) {
    return axios.post('updateInstruccionModulo', data)
}
export function postCarreraSimulacro (data) {
    return axios.post('postCarreraSimulacro', data)
}

export function updateAnimacion (data) {
    return axios.post('updateAnimacion', data)
}
export function postUpdateClave (data) {
    return axios.post('postUpdateClave', data)
}
export function postSimulacro (data) {
    return axios.post('postSimulacro', data)
}
export function updateSimulacro (data) {
    return axios.post('updateSimulacro', data)
}
export function updateSimulacroTipo (data) {
    return axios.post('updateSimulacroTipo', data)
}
export function postNewExamen (data) {
    return axios.post('postNewExamen', data)
}

export function getAsesores () {
    return axios.get('getAsesores')
}
export function getSimulacroId (id) {
    return axios.get('getSimulacroId/' + id)
}

export function getModulosSimulacros (id) {
    return axios.get('getModulosSimulacros/' + id)
}
export function getCarreraSimulacro (id) {
    return axios.get('getCarreraSimulacro/' + id)
}
export function getPreguntasModulo (id) {
    return axios.get('getPreguntasModulo/' + id)
}

/*export function getPromedio (data) {
    return axios.get('getPromedio', {
        params: data
    })
}*/

