import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import CoreuiVue from '@coreui/vue'
import configs from './local_config'
import BootstrapVue from 'bootstrap-vue'
import vSelect from 'vue-select'
import VueAwesomeSwiper from 'vue-awesome-swiper'


Vue.use(require('vue-moment'));
import 'swiper/css/swiper.css'

Vue.use(VueAwesomeSwiper)
Vue.component('v-select', vSelect)

import 'vue-select/dist/vue-select.css';
import './assets/scss/app.scss'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-slick-carousel/dist/vue-slick-carousel-theme.css'

const Config = require('./config')
Vue.prototype.$eventBus = new Vue() // add this line of code

Vue.use(BootstrapVue)
Vue.use(CoreuiVue)
Vue.use(Config, configs)

Vue.config.productionTip = configs.PRODUCTION

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
