import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

const Inicio = () => import('@/views/Home')
const HomeStudent = () => import('@/views/Student/layout')
const Result = () => import('@/views/Student/Resultados')
const Help = () => import('@/views/Student/Help')
const Register = () => import('@/views/Student/Register')
const Simulacro = () => import('@/views/Student/Simulacro')
const Simulacros = () => import('@/views/Student/Simulacros')
const Instruccion = () => import('@/views/Student/Instruccion')
const Edit = () => import('@/views/Student/EditStudent')
const Calificar = () => import('@/views/Student/Calificar')
const HomeAsesor= () => import('@/views/Adviser/layout')
const Dashboard= () => import('@/views/Adviser/Dashboard')
const HomeColegio= () => import('@/views/School/layout')
const Welcome= () => import('@/views/School/Welcome')
const Results= () => import('@/views/School/Results')
const Keys= () => import('@/views/Adviser/Keys')
const Data= () => import('@/views/Adviser/Data')
const DataEstudiantes= () => import('@/views/Adviser/DataEstudiantes')
const ResultsStudent= () => import('@/views/School/ResultsStudent')
const ListSimulacros= () => import('@/views/School/ListSimulacros')
const ListSimulacrosAdviser= () => import('@/views/Adviser/ListSimulacros')
const ListSimulacrosAdviser1= () => import('@/views/Adviser/ListSimulacros1')

const ReportSimulacro= () => import('@/views/Adviser/ReportSimulacro')
const ReportConsult= () => import('@/views/Adviser/ReportConsult')

const Support= () => import('@/views/School/Support')
const Reportes= () => import('@/views/Adviser/Reportes')
const ResultsGroupGraf= () => import('@/views/School/ResultsGroupGraf')
const DetResultsStudent= () => import('@/views/School/DetResultStudent')
const DetResultsStudentAdviser= () => import('@/views/Adviser/DetResultStudent')
const ResultsStudentAdviser= () => import('@/views/Adviser/ResultsStudent')

const Logindmin = () => import('@/views/Admin/Home')
const HomeAdmin = () => import('@/views/Admin/Layout')
const DashboardAdmin = () => import('@/views/Admin/Dashboard')
const Account = () => import('@/views/Admin/Account')
const AdminData = () => import('@/views/Admin/Data')
const Information = () => import('@/views/Admin/Information')
const AdminKeys = () => import('@/views/Admin/Keys')
const AdminResult = () => import('@/views/Admin/Result')
const AdminSimulacros = () => import('@/views/Admin/Simulacro')
const newSimulacro = () => import('@/views/Admin/NewSimulacro')
const ListSimulacrosAdmin= () => import('@/views/Admin/ListSimulacros')
const DataEstudiantesAdmin= () => import('@/views/Adviser/DataEstudiantes')
const LoadInfomation = () => import('@/views/Admin/LoadInfomation')
const EditInfomation = () => import('@/views/Admin/EditInfomation')
const EditInfomationForm = () => import('@/views/Admin/EditInfomationForm')
const AdminClaves = () => import('@/views/Admin/EditClaves')
const editSimulacro = () => import('@/views/Admin/editSimulacro')
const editSimulacroTipo = () => import('@/views/Admin/EditSimulacroTipo')
const newTipoSimulacro = () => import('@/views/Admin/newTipoSimulacro')




Vue.use(Router)

function configRoutes() {
    return [
        {
            path: '/',
            name: 'Inicio',
            component: Inicio,
        },
        {
            path: '/register',
            name: 'Register',
            component: Register,
        },
        {
            path: '/simulacros',
            name: 'Simulacros',
            component: Simulacros,
            
        },
        {
            path: '/student',
            name: 'Student',
            component: HomeStudent,
            redirect: '/result',
            beforeEnter(to, from, next) {
                if (!store.getters.isAuthenticated) {
                    next('/')
                } else {
                    next()
                }
            },
            children: [
                {
                 
                    path: '/result',
                    name: 'Result',
                    component: Result,
                },
                {
                    path: '/instruccion',
                    name: 'Instruccion',
                    component: Instruccion,               
                },
                {
                    path: '/Verificar-datos',
                    name: 'Edit',
                    component: Edit,               
                },
                {
                    path: '/Calificar',
                    name: 'Calificar',
                    component: Calificar,               
                },
                {
                    path: '/simulacro',
                    name: 'Simulacro',
                    component: Simulacro,
                    props: { default: true },
                    
                },
                {
                    path: '/help',
                    name: 'Help',
                    component: Help,
                }
            ]
        },
        {
            path: '/asesor',
            name: 'Asesor',
            component: HomeAsesor,
            redirect: '/asesor/dashboard',
            beforeEnter(to, from, next) {
                if (!store.getters.isAuthenticated || !store.getters.isAsesor) {
                    next('/')
                } else {
                    next()
                }
            },
            children: [
                {
                 
                    path: '/asesor/dashboard',
                    name: 'Dashboard',
                    component: Dashboard,
                },
                {
                 
                    path: '/asesor/keys',
                    name: 'Keys',
                    component: Keys,
                },
                {
                 
                    path: '/asesor/reportes',
                    name: 'Resportes',
                    component: Reportes,
                },
                {
                 
                    path: '/asesor/data',
                    name: 'Data',
                    component: Data,
                },
                {
                    path: '/asesor/data/:id',
                    name: 'Data Estudiantes',
                    component: DataEstudiantes,
                },
                {
                    path: '/asesor/list-simulacros',
                    name: 'Listado-Simulacros-Tipo',
                    component: ListSimulacrosAdviser,
                },
                {
                    path: '/asesor/list-simulacros-1',
                    name: 'Listado Simulacros colegio Grupo',
                    component: ListSimulacrosAdviser1,
                },
                {
                    path: '/asesor/report-consulta',
                    name: 'Reporte-Consulta',
                    component: ReportConsult,
                },
                {
                    path: '/asesor/report-simulacros',
                    name: 'Reporte-Simulacro',
                    component: ReportSimulacro,
                },
                {
                    path: '/asesor/resultados-grupo',
                    name: 'ResultsGraficasGrupo',
                    component: ResultsGroupGraf,
                },
                {
                    path: '/asesor/resultados-estudiante',
                    name: 'Resultados por estudiante',
                    component: DetResultsStudentAdviser,
                },
                {
                    path: '/asesor/resultados-estudiantes',
                    name: 'Resultados por estudiante colegio',
                    component: ResultsStudentAdviser,
                }
                
            ]
        },
        {
            path: '/colegio',
            name: 'Colegio',
            component: HomeColegio,
            redirect: '/colegio/bienvenido',
            beforeEnter(to, from, next) {
                if (!store.getters.isAuthenticated || !store.getters.isColegio) {
                    next('/')
                } else {
                    next()
                }
            },
            children: [
                {
                    path: '/colegio/bienvenido',
                    name: 'Bienvenido',
                    component: Welcome,
                },
                {
                    path: '/colegio/resultados',
                    name: 'Resultados',
                    component: Results,
                },
                {
                    path: '/colegio/resultados-estudiantes',
                    name: 'Resultados por estudiantes del colegio',
                    component: ResultsStudent,
                },
                {
                    path: '/colegio/resultados-estudiante',
                    name: 'Resultados por estudiante colegio 2',
                    component: DetResultsStudent,
                },
                {
                    path: '/colegio/resultados-grupo',
                    name: 'ResultsGraficasGrupoColegio',
                    component: ResultsGroupGraf,
                },
                {
                    path: '/colegio/list-simulacros',
                    name: 'Listado Simulacros colegio',
                    component: ListSimulacros,
                },
                {
                    path: '/colegio/soporte',
                    name: 'Soporte colegio',
                    component: Support,
                },
                
            ]
        },
        {
            path: '/admin',
            name: 'Admin',
            component: Logindmin,
        },
        {
        path: '/admin/home',
        name: 'Admin',
        component: HomeAdmin,
        redirect: '/admin/dashboard',
        beforeEnter(to, from, next) {
            if (!store.getters.isAuthenticated || !store.getters.isAdmin) {
                next('/admin')
            } else {
                next()
            }
        },
        children: [
            {
                path: '/admin/dashboard',
                name: 'Bienvenido2',
                component: DashboardAdmin,
            },
            {
                path: '/admin/account',
                name: 'Gestion de cuentas',
                component: Account,
            },
            {
                path: '/admin/keys',
                name: 'Gestion de keys',
                component: AdminKeys,
            },
            {
                path: '/admin/Information',
                name: 'Carga de informacion',
                component: Information,
            },
            {
                path: '/admin/Information/edit',
                name: 'cargar-informacion',
                component: LoadInfomation,
            },
            {
                path: '/admin/Information/editInfo',
                name: 'editar-informacion',
                component: EditInfomation,
                props: true,
            },
            {
                path: '/admin/Information/editInfo/:id',
                name: 'editar-informacion-id',
                component: EditInfomationForm
            },
            {
                path: '/admin/result/reportes',
                name: 'Resultados2',
                component: AdminResult,
            },
            {
                path: '/admin/Information/claves',
                name: 'editar-claves',
                component: AdminClaves,
            },
            
            {
                path: '/admin/simulacros',
                name: 'Simulacros2',
                component: AdminSimulacros,
            },
            {
                path: '/admin/data',
                name: 'Data',
                component: AdminData,
            },
            {
                path: '/admin/simulacro/new',
                name: 'NewSimulacro',
                component: newSimulacro,
            },
            {
                path: '/admin/simulacro/newtipo',
                name: 'NewTipoSimulacro',
                component: newTipoSimulacro,
            },
            
            {
                path: '/admin/simulacro/edit',
                name: 'NewSimulacro',
                component: editSimulacro,
            },
            {
                path: '/admin/simulacro/editTipo',
                name: 'Editar simulacro  tipo',
                component: editSimulacroTipo,
            },
            
            {
                path: '/admin/result/list-simulacros',
                name: 'Listado Simulacros colegio Grupo',
                component: ListSimulacrosAdmin,
            },
            {
                path: '/admin/result/resultados-estudiantes',
                name: 'Resultados por estudiante colegio',
                component: ResultsStudentAdviser,
            },
            {
                path: '/admin/result/list-simulacros-1',
                name: 'Listado-Simulacros-Tipo-Admin',
                component: ListSimulacrosAdviser,
            },
            {
                path: '/admin/result/report-consulta',
                name: 'Reporte-Consulta-Admin',
                component: ReportConsult,
            },
            {
                path: '/admin/result/report-simulacros',
                name: 'Reporte-Simulacro-Admin',
                component: ReportSimulacro,
            },
            {
                path: '/admin/data/:id',
                name: 'Data Estudiantes',
                component: DataEstudiantesAdmin,
            },
            
        ]
    }
    ]
}

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: configRoutes()
})

