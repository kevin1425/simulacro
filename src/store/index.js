import Vue from 'vue'
import Vuex from 'vuex'
//import moment from 'moment'
import {isValidJwt, setHeaderAuth} from '@/utils'

Vue.use(Vuex)

import {
    authenticateEstudiante,
    VerifyKeys,
    searchCollege,
    searchCareer,
    registerStudent,
    getResultados,
    getPromedio,
    getPuntajesMaximo,
    getStudiantesPorCarrera,
    getSimulacro,
    getPreguntas,
    getInstruccionesGenereales,
    getInstruccionesModulo,
    getAnimacionesSimulacro,
    getAnimacionesResultado,
    actualizarEstudiante,
    postRespuesta,
    getModulosxSimulacros,
    PostCalificarSimulcro,
    authenticateAsesor,
    authenticateColegio,
    getDashboard,
    getPromedioSimulacros,
    GetKeyAsesor,
    GetEstudiante,
    GetKeysColegio,
    GetStudentsKey,
    GetEstudiantesStatus,
    getSecciones,
    getGrados,
    postRegisterKeys,
    deleteKeys,
    updateKeys,
    GetMaxMinSimulacro,
    GetColegioKey,
    postUpdateKeys,
    GetNumeroEstudiante,
    GetAvgCarrera,
    GetBuenMalPuntaje,
    PostSupport,
    loginAdmin,
    getAsesores,
    GetKeysAsesors,
    GetColegioKeys,
    getDashboarddatas,
    getPromedioSimulacroGraf,
    getDificultModule,
    getSatifaccion,
    postRegisterAccount,
    GetAccounts,
    updateAccount,
    updateAPassword,
    deleteAccount,
    sendCredentials,
    postOption,
    getOption,
    getSimulacrosXcarrera,
    GetKeysAsesorAll,
    updateInstruccion,
    updateAnimacion,
    getModulosSimulacros,
    getCarreraSimulacro,
    getPreguntasModulo,
    postUpdateClave,
    postSimulacro,
    getSimulacroId,
    updateSimulacro,
    getSimulacroTipo,
    getSimulacrosAdmin,
    updateSimulacroTipo,
    postNewExamen,
    eliminarSimulacroTipo,
    postInfo,
    uploadImage,
    updateInstruccionModulo,
    postCarreraSimulacro,
    getInstruccionesModuloExamen,
    eliminarSimulacro
} from '@/api'

const state = {
    user: {},
    student: {},
    keyAll: {},
    jwt: null,
    key: '',
    color: '#0F376E',
    id_simulacro: null,
    nombre_modulo: '',
    start: '',
    nav: '',
    search: '',
    tipo:null,
    activeModal: false,
    activeModalSend: false,
    activeRouteButton: ''
}

const mutations = {
    setUserData(state, payload) {
        state.user = payload
        //localStorage.full_name = payload.nombre
        localStorage.userdata = JSON.stringify(payload)
        localStorage.id_estudiante = payload.id
        localStorage.tipo = payload.tipo
        state.tipo = payload.tipo

        console.log('id_estudiante:', payload)
        localStorage.keys = payload.key_simulacro
    },
    setJwtToken(state, payload) {
        localStorage.token = payload
        setHeaderAuth()
        state.jwt = payload
    },
    setname(state, payload) {
        setHeaderAuth()
        localStorage.nombres = payload.nombres
        localStorage.apellido = payload.apellidos
     },
    setDataStudent(state, payload) {
        setHeaderAuth()
        localStorage.userdata = JSON.stringify(payload.user)
        state.user = payload.user
        state.student = payload.estudiante
        console.log('id_estudiante:', payload.estudiante)
        localStorage.estudiante = JSON.stringify(payload.estudiante)
        localStorage.id_estudiante = payload.estudiante.id
        localStorage.carrera = payload.carrera.nombre
        localStorage.id_carrera = payload.carrera.id
    },
    setColor(state, payload) {
        localStorage.color = payload
        state.color = payload
    },
    start(state, payload) {
        state.start = payload
    },
    logOut(state) {
        state.user = null
        state.jwt = null
        state.color = null
        state.key = null
        state.keyAll = null
        state.keyAll = null
        state.student = null

    },
    setModulo(state, payload) {
        localStorage.nombre_modulo = payload
        state.nombre_modulo = payload
    },
    setSearch(state, payload) {
        
        state.search = payload
        localStorage.search = payload

    },
    setKey(state, payload) {
        localStorage.key = payload.key
        state.key = payload.key
        localStorage.keyAll = JSON.stringify(payload)
        localStorage.keys = payload
        state.keyAll = payload
    },
    setSimulacros(state, payload) {
        localStorage.id_simulacro = payload
        console.log('setSimulacros:', payload)
        state.id_simulacro = payload
    },
    setNav(state, payload) {
        state.nav = payload
        localStorage.nav = payload
    },
    setKeyData(state, payload) {
        localStorage.keyData = JSON.stringify(payload)
    },
    setActiveModal(state, payload) {
        state.activeModal = payload
    },
    setActiveModalSend(state, payload) {
        state.activeModalSend = payload
    },
    setActiveRouteButton(state, payload) {
        state.activeRouteButton = payload
    },
}
const actions = {
    loginAdmin(context, data) {
        return loginAdmin(data)
    },
    loginEstudiante(context, data) {
        return authenticateEstudiante(data)
    },
    loginAsesor(context, data) {
        return authenticateAsesor(data)
    },
    loginColegio(context, data) {
        return authenticateColegio(data)
    },
    getPromedioSimulacroGraf(context, data) {
        console.log('aqui **********data', data)
        return getPromedioSimulacroGraf(data)
    },
    verifykey(context, id) {
        return VerifyKeys(id)
    },
    getCollege() {
        return searchCollege()
    },
    getCareer() {
        return searchCareer()
    },
    getSimulacros() {
        return getSimulacro()
    },
    getSimulacrosAdmin() {
        return getSimulacrosAdmin()
    },
    getGrado() {
        return getGrados()
    },
    getSeccion() {
        return getSecciones()
    },
    getAsesores() {
        return getAsesores()
    },
    GetAccounts() {
        return GetAccounts()
    },
    getResultado(context, id) {
        return getResultados(id)
    },
    getSimulacrosXcarrera(context, id) {
        return getSimulacrosXcarrera(id)
    },
    getSimulacroId(context, id) {
        return getSimulacroId(id)
    },
    getSimulacroTipo(context, data) {
        return getSimulacroTipo(data)
    },
    
    getOption(context, id) {
        return getOption(id)
    },
    getPregunta(context, data) {
        return getPreguntas(data)
    },
    GetStudentKey(context, id) {
        return GetStudentsKey(id)
    },

    getModulosxSimulacro(context, id) {
        return getModulosxSimulacros(id)
    },
    postRegisterStudent(context, data) {
        return registerStudent(data)
    },
    postSimulacro(context, data) {
        return postSimulacro(data)
    },
    updateSimulacro(context, data) {
        return updateSimulacro(data)
    },
    updateSimulacroTipo(context, data) {
        return updateSimulacroTipo(data)
    },
    
    postRegisterKey(context, data) {
        return postRegisterKeys(data)
    },
    postRegisterAccount(context, data) {
        return postRegisterAccount(data)
    },
    postUpdateKey(context, data) {
        return postUpdateKeys(data)
    },
    postActualizarEstudiante(context, data) {
        return actualizarEstudiante(data)
    },
    postCarreraSimulacro(context, data) {
        return postCarreraSimulacro(data)
    },
    
    postNewExamen(context, data) {
        return postNewExamen(data)
    },
    PostCalificar(context, data) {
        return PostCalificarSimulcro(data)
    },
    postRespuestas(context, data) {
        return postRespuesta(data)
    },
    postOption(context, data) {
        return postOption(data)
    },
    deleteKey(context, data) {
        return deleteKeys(data)
    },
    updateKey(context, data) {
        return updateKeys(data)
    },
    updateAccount(context, data) {
        return updateAccount(data)
    },
    updateAPassword(context, data) {
        return updateAPassword(data)
    },
    sendCredentials(context, data) {
        return sendCredentials(data)
    },
    
    deleteAccount(context, data) {
        return deleteAccount(data)
    },
    setKey(context, key) {
        context.commit('setKey', key)
    },
    setKeyData(context, key) {
        context.commit('setKeyData', key)
    },
    setModulo(context, modulo) {
        context.commit('setModulo', modulo)
    },
    setSearch(context, value) {
        context.commit('setSearch', value)
    },
    setColor(context, color) {
        context.commit('setColor', color)
    },
    setSimulacros(context, id) {
        context.commit('setSimulacros', id)
    },
    setNav(context, value) {
        context.commit('setNav', value)
    },
    setStudent(context, data) {
        context.commit('setDataStudent', data)
    },
    setToken(context, key) {
        context.commit('setJwtToken', key)
    },
    getPromedios(context, data) {
        return getPromedio(data)
    },
    getPromedioSimulacro(context, data) {
        return getPromedioSimulacros(data)
    },
    getPuntajeMaximo(context, id) {
        return getPuntajesMaximo(id)
    },
    getDashboarddata(context, id) {
        return getDashboard(id)
    },
    eliminarSimulacroTipo(context, data) {
        return eliminarSimulacroTipo(data)
    },
    eliminarSimulacro(context, data) {
        return eliminarSimulacro(data)
    },
    
    getDashboarddatas() {
        return getDashboarddatas()
        },
    GetKeysAsesor(context, id) {
        return GetKeyAsesor(id)
    },
    GetKeysAsesores() {
        return GetKeysAsesors()
    },
    GetKeysAsesorAll() {
        return GetKeysAsesorAll()
    },
    GetColegiosKey(context, id) {
        return GetColegioKey(id)
    },
    getSatifaccion(context, data) {
        return getSatifaccion(data)
    },
    
    GetColegiosKeys() {
        return GetColegioKeys()
    },
    getStudiantePorCarrera(context, id) {
        return getStudiantesPorCarrera(id)
    },
    getInstruccionesGenereales(context, id) {
        return getInstruccionesGenereales(id)
    },
    getInstruccionesModulos(context, id) {
        return getInstruccionesModulo(id)
    },
    getInstruccionesModuloExamen(context, id) {
        return getInstruccionesModuloExamen(id)
    },

    getAnimacionesSimulacros(context, id) {
        return getAnimacionesSimulacro(id)
    },
    updateInstruccionModulo(context, data) {
        return updateInstruccionModulo(data)
    },
    
    
    getAnimacionesResultados(context, id) {
        return getAnimacionesResultado(id)
    },
    getEstudiante(context, id) {
        return GetEstudiante(id)
    },
    getKeysColegio(context, id) {
        return GetKeysColegio(id)
    },
    getEstudiantesStatus(context, id) {
        return GetEstudiantesStatus(id)
    },
    getMaxMinSimulacro(context, data) {
        return GetMaxMinSimulacro(data)
    },
    getNumeroEstudiante(context, data) {
        return GetNumeroEstudiante(data)
    },
    getAvgCarrera(context, data) {
        return GetAvgCarrera(data)
    },
    getDificultModule(context, data) {
        return getDificultModule(data)
    },
    
    getModulosSimulacros(context, id) {
        return getModulosSimulacros(id)
    },
    getCarreraSimulacro(context, id) {
        return getCarreraSimulacro(id)
    },   
    getPreguntasModulo(context, id) {
        return getPreguntasModulo(id)
    },  
    
    getBuenMalPuntaje(context, data) {
        return GetBuenMalPuntaje(data)
    },
    postSupport(context, data) {
        return PostSupport(data)
    },
    uploadImage(context, data) {
        return uploadImage(data)
    },
    updateInstruccion(context, data) {
        return updateInstruccion(data)
    },
    updateAnimacion(context, data) {
        return updateAnimacion(data)
    },
    postUpdateClave(context, data) {
        return postUpdateClave(data)
    },
    postInfo(context, data) {
        return postInfo(data)
    },
    
    
    setActiveModal(context, key) {
        context.commit('setActiveModal', key)
    },
    setActiveModalSend(context, key) {
        context.commit('setActiveModalSend', key)
    },
    setActiveRouteButton(context, route) {
        context.commit('setActiveRouteButton', route)
    },
};
const getters = {

    isAuthenticated(state) {
        let token
        if (state.jwt) {
            token = state.jwt
        } else {
            token = localStorage.getItem('token')
        }
        // console.log("is valids is", isValidJwt(token))

        return isValidJwt(token)
    },
    isAdmin(state) {
        let tipo
        if (state.tipo) {
            tipo = state.tipo
        } else {
            tipo = localStorage.getItem('tipo')
        }

        return tipo==1 ? true:false
    },
    isColegio(state) {
        let tipo
        if (state.tipo) {
            tipo = state.tipo
        } else {
            tipo = localStorage.getItem('tipo')
        }

        return tipo==3 ? true:false
    },
    isAsesor(state) {
        let tipo
        if (state.tipo) {
            tipo = state.tipo
        } else {
            tipo = localStorage.getItem('tipo')
        }

        return tipo==2 ? true:false
    },
    isKeyValid(state) {
        let key = false
        if (state.key != '' || !!localStorage.getItem('key')) {
            key = true
        }
        console.log("is state key", state.key)
        console.log("is localStorage", localStorage.getItem('key'))
        console.log("is key is", key)
        return key
    }

}

const store = new Vuex.Store({
    state,
    actions,
    mutations,
    getters
})

export default store
